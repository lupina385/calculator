tworzymy kalkulator
1 utorz projekt w GitLab
2 zaznacz stworzenie pliku Readme w czasie tworzenia projektu
3 sciagnij projekt do siebie (clone)
4 stworz modul main
5 dodaj gitignore - uzupelnic
6 wyslij zmiany na serwer
7 dodaj modul calc_helper
8 w calc_helper utworz funkcje (add, sub, multi, div) - parametry wejsciowe oraz wyjsciowe
9 w module main utworz menu 1 Dodaj, 2 Odejmij, 3 Pomnoz, 4 Podziel, 0 wyjscie
10 uworz funkcje w main ktora bedzie pobierac wartosc od urzytkownika (mozna dodac walidacje)
11 na wywolaniu kazdego wyboru uruchamiamy funkcje z modulu calc_helper (np > calc_helper.add(2, 3))
12 wynik funkcji wyswietlamy na ekranie
